const app = Vue.createApp({
  data() {
    return {
      friends: [
        {
          id: "Laurent",
          name: "Laurent Sauzet",
          phone: "06 12 34 56 78",
          email: "laurent.sauzetpro@gmail.com",
        },
        {
          id: "Julie",
          name: "Julie Jones",
          phone: "06 32 65 98 87",
          email: "julie.jones@gmail.com",
        },
      ],
    };
  },
});

app.component("friend-contact", {
  template: `
  <li>
  <h2>{{friend.name}}</h2>
  <button @click="toggleDetails">
    {{ detailsAreVisible ? 'Hide': 'Show' }} Details
  </button>
  <ul v-if="detailsAreVisible">
    <li><strong>Phone:</strong> {{friend.phone}}</li>
    <li><strong>Email:</strong> {{friend.email}}</li>
  </ul>
</li>
`,
  data() {
    return {
      detailsAreVisible: false,
      friend: {
        id: "Laurent",
        name: "Laurent Sauzet",
        phone: "06 12 34 56 78",
        email: "laurent.sauzetpro@gmail.com",
      },
    };
  },
  methods: {
    toggleDetails() {
      this.detailsAreVisible = !this.detailsAreVisible;
    },
  },
});

app.mount("#app");
